'use strict';

/**
* Change into specs folder so that file loading works.
*/
const chai = require('chai');

describe('txl-account-service', function() {

	describe('/v1/account', function() {
		it('/balance', function(done) {
			chai.request(this.basicUrl)
					.get('/v1/account/balance')
					.end((err, res) => {
							chai.expect(res.status).to.equal(200);
							chai.expect(res.statusCode).to.equal(200);
							chai.expect(res.body.result.opGetChildList).to.be.a('object');
							chai.expect(res.body.result.opGetChildList.headerRs.responseCode).equal('00');
							if(res.body.result.opGetChildList.headerRs.responseCode === '00') {
									chai.expect(res.body.result.opGetChildList.childList).to.be.a('array');
							}
							done();
					});
		});
  });
});

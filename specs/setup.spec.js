const sinon = require('sinon');
const chai = require('chai');
const chaiHttp = require('chai-http');
const sinonChai = require('sinon-chai');
const service = require('../txl-account-service');
const config = require('fwsp-config');

before(function(done) {
  chai.use(sinonChai);
  chai.use(chaiHttp);
  this.basicUrl = 'http://'+ config.hydra.serviceIP +':'+ config.hydra.servicePort;
  service.init().then(() => {
    done();
  });
});

beforeEach(function() {
  this.sandbox = sinon.createSandbox();
});

afterEach(function() {
  this.sandbox.restore();
});
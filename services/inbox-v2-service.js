const hydraExpress = require('hydra-express');
const stringify = require('jsesc');
const hystrixutil = require('@xl/hystrix-util');
const config = require('fwsp-config').getObject();
const axios = require('axios');
const masheryhttp = require('@xl/mashery-http');
const jClone = require('@xl/json-clone');
const aes256 = require('@xl/custom-aes256');
const indirectObject = require('@xl/indirect-object-handler');
const apicache = require('@xl/apicache');
const Moment = require('moment');
const MomentRange = require('moment-range');
const rc = require('../constant/RC');
const apigatehttp = require('@xl/apigate-http');

const moment = MomentRange.extendMoment(Moment);

const beCipher = new aes256();
beCipher.createCipher(config.pin.backend);

module.exports = {
  list: (req, res) => {
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Request', message: 'Request inbox'}));
    const { user } = req;
    const fallbackOptions = {};
    const fallback = (error, args) => {
      return Promise.reject({
        message: error.message,
        args
      });
    };
    const lang = req.headers.language;
    const authHeader = req.headers.authorization;
    const { initiator } = req.headers;
    const bypassForce = req.headers['x-apicache-bypass'] ? req.headers['x-apicache-bypass'] : 'false';
    const startDate = req.query.startdate;
    const endDate = req.query.enddate;
    const { q } = req.query;
    if(initiator === 'local') {
      const requestOptions = jClone.clone(config.apis['inboxallnew']);
      requestOptions.headers.event_id = req.logId;
      requestOptions.headers.actor = user.msisdn;
      const querys = [];
      querys.push(['msisdn', user.msisdn]);
      hystrixutil.request('inboxallnew', requestOptions, apigatehttp.request, fallbackOptions, fallback, querys).then(result => {
        if(result && result.data && result.data.mailboxes) {
          hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Get inbox all', message: 'Get inbox all success'}));
          const forbiddenContactSources = ['cpConfirmationManagement'];
          let allowedData = [];
          if(Array.isArray(result.data.mailboxes) && result.data.mailboxes.length > 0) {
            result.data.mailboxes.forEach(elm => {
              if(!(elm.readFlag === 'DELETED' || forbiddenContactSources.includes(elm.contactSource))) {
                elm = module.exports.replaceNewContactPropWithOldProp(elm);
                allowedData.push(elm);
              }
            });
          } else {
            allowedData = [];
          }
          res.sendOk({errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data: allowedData});
        } else {
          hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Get inbox all', message: 'Get inbox all failed', data: result.data}));
          hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Reach API failed'}));
          res.sendError({errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm});
        }
      }).catch(error => {
        console.error(error);
        hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Get inbox all', message: 'Error getting inbox all', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Reach API failed'}));
        res.sendError({errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm});
      });
    } else {
      const requestOptions = jClone.clone(config.apis['selfinboxall']);
      requestOptions.headers.event_id = req.logId;
      requestOptions.headers.actor = user.msisdn;
      const querys = [];
      querys.push(['authHeader', authHeader]);
      querys.push(['bypassForce', bypassForce]);
      hystrixutil.request('selfinboxall', requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Call inbox all', message: 'Calling self inbox all success'}));
        const data = result && result.data && result.data.result && result.data.result.data ? result.data.result.data : [];
        const allowedData = [];
        if(Array.isArray(data) && data.length > 0) {
          for(let i = 0; i < data.length; i++) {
            const elm = data[i];
            if(module.exports.compareDate(elm.timestamp, startDate, endDate) && (!q || q && elm.message.toLowerCase().indexOf(q.toLowerCase()) >= 0)) {
              allowedData.push(elm);
            }
          }
        }
        res.sendOk({errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data: allowedData});
      }).catch(error => {
        hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Call inbox all', message: 'Error calling self inbox all', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Reach API failed'}));
        res.sendError({errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm});
      });
    }    
  },
  detail: (req, res) => {
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Request', message: 'Request detail inbox'}));
    const { user } = req;
    const fallbackOptions = {};
    const fallback = (error, args) => {
      return Promise.reject({
        message: error.message,
        args
      });
    };
    const lang = req.headers.language;
    const authHeader = req.headers.authorization;
    const bypassForce = req.headers['x-apicache-bypass'] ? req.headers['x-apicache-bypass'] : 'false';
    const requestOptions = jClone.clone(config.apis['selfinboxall']);
    requestOptions.headers.event_id = req.logId;
    requestOptions.headers.actor = user.msisdn;
    const { mailboxid } = req.params;
    const encMailboxId = indirectObject.encrypt(beCipher, mailboxid, null);
    const querys = [];
    querys.push(['msisdn', user.msisdn]);
    querys.push(['mailboxid', mailboxid]);
    querys.push(['authHeader', authHeader]);
    querys.push(['bypassForce', bypassForce]);
    hystrixutil.request('selfinboxall', requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
      if(result && result.data && result.data.result && result.data.result.data) {
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Get detail inbox', message: 'Get detail inbox success'}));
        let data = {};
        if(Array.isArray(result.data.result.data) && result.data.result.data.length > 0) {
          for(let i = 0; i < result.data.result.data.length; i++) {
            const elm = result.data.result.data[i];
            if(elm.mailboxid === encMailboxId) {
              data = elm;
              break;
            }
          }
          if(data.flag === 'UNREAD') {
            data.flag = 'READ';
            apicache.clear('/v1/inbox$$appendKey='+ user.msisdn);
            module.exports.detailV1(req);
          }
        }
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Get detail inbox success'}));
        res.sendOk({errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data});
      } else {
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Get detail inbox', message: 'Get detail inbox failed', data: result.data}));
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Reach API failed'}));
        res.sendError({errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm});
      }
    }).catch(error => {
      hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Get detail inbox', message: 'Error getting detail inbox', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error}));
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Reach API failed'}));
      res.sendError({errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm});
    });
  },
  delete: (req, res) => {
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Request', message: 'Delete inbox'}));
    const { user } = req;
    const fallbackOptions = {};
    const fallback = (error, args) => {
      return Promise.reject({
        message: error.message,
        args
      });
    };
    const lang = req.headers.language;
    const { mailboxid } = req.params;
    const requestOptions = jClone.clone(config.apis['inboxdelete']);
    requestOptions.headers.event_id = req.logId;
    requestOptions.headers.actor = user.msisdn;
    const querys = [];
    querys.push(['msisdn', user.msisdn]);
    querys.push(['mailboxid', mailboxid]);
    hystrixutil.request('inboxdelete', requestOptions, apigatehttp.request, fallbackOptions, fallback, querys).then(result => {
      if(result && result.status === 200 && result.data === '') {
        apicache.clear('/v1/inbox$$appendKey='+ user.msisdn);
        apicache.clear('/v1/inbox/'+ req.params.mailboxid +'$$appendKey='+ user.msisdn);
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Delete inbox', message: 'Delete inbox success'}));
        res.sendOk({errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm});
      } else {
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Delete inbox', message: 'Delete inbox failed'}));
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Process failed'}));
        res.sendError({errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm});
      }
    }).catch(error => {
      hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Delete inbox', message: 'Error deleting inbox', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Reach API failed'}));
      res.sendError({errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm});
    });
  },
  cacheOptions: {
    appendKey: (req, res) => {
      return req.user.msisdn;
    },
    headers: {
      'cache-control': 'no-cache, no-store, must-revalidate',
      'pragma': 'no-cache',
      'expires': 0
    }
  },
  getCacheToggle: (req, res) => {
    return req && req.user && req.user.msisdn && req.query.startdate === undefined && req.query.enddate === undefined && req.query.q === undefined;
  },
  getCacheDurationInMillis: (req, res) => {
    const today = new Date();
    const seconds = today.getSeconds();
    const minutesToSeconds = today.getMinutes() * 60;
    const hoursToSeconds = today.getHours() * 60 * 60;
    const result = hoursToSeconds + minutesToSeconds + seconds;
    return (86400 - result) * 1000;
  },
  compareDate: (date, startDate, endDate) => {
    if(startDate) {
      startDate = moment((startDate.length > 10 ? startDate.substring(0, 10) : startDate) + ' 00:00:00');
      endDate = endDate ? moment((endDate.length > 10 ? endDate.substring(0, 10) : endDate) + ' 23:59:59') : moment();
      const range = moment.range(startDate, endDate);
      date = moment(date.replace('T', ' '));
      
      return range.contains(date);
    } else {
      return true;
    }
  },
  detailV1: (req) => {
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Get detail inbox', message: 'Request detail inbox'}));
    const { user } = req;
    const fallbackOptions = {};
    const fallback = (error, args) => {
      return Promise.reject({
        message: error.message,
        args
      });
    };
    const requestOptions = jClone.clone(config.apis['inboxdetail']);
    requestOptions.headers.event_id = req.logId;
    requestOptions.headers.actor = user.msisdn;
    const { mailboxid } = req.params;
    const querys = [];
    querys.push(['msisdn', user.msisdn]);
    querys.push(['mailboxid', mailboxid]);
    hystrixutil.request('inboxdetail', requestOptions, apigatehttp.request, fallbackOptions, fallback, querys).then(result => {
      if(result && result.data && result.data.servicesource && result.data.contactsource) {
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Get detail inbox', message: 'Get detail inbox success', data: result.data}));
      } else {
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Get detail inbox', message: 'Get detail inbox failed', data: result.data}));
      }
    }).catch(error => {
      hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Get detail inbox', message: 'Error getting detail inbox', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
    });
  },
  replaceNewContactPropWithOldProp: (elm) => {
    const newElm = {};
    newElm.mailboxid = indirectObject.encrypt(beCipher, elm.mailboxId + '', null);
    newElm.briefmessage = elm.briefMessage;
    newElm.message = elm.message;
    if(elm.timestamp && elm.timestamp.length > 25) {
      elm.timestamp = elm.timestamp.substring(0, 19) + elm.timestamp.substring(23, 26) +':'+ elm.timestamp.substring(26, 28);
    }
    newElm.timestamp = elm.timestamp;
    newElm.flag = elm.readFlag;
    newElm.servicesource = elm.serviceId;
    newElm.contactsource = elm.contactSource;
    newElm.condition = elm.notificationCondition;
    newElm.status = elm.lastReasonCode;

    return newElm;
  }
}
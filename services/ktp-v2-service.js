const hydraExpress = require('hydra-express');
const stringify = require('jsesc');
const hystrixutil = require('@xl/hystrix-util');
const config = require('fwsp-config').getObject();
const uuidv1 = require('uuid/v1');
const axios = require('axios');
const masheryhttp = require('@xl/mashery-http');
const aes256 = require('@xl/custom-aes256');
const jClone = require('@xl/json-clone');
const dateutil = require('@xl/util-date');
const rc = require('../constant/RC');
const apigatehttp = require('@xl/apigate-http');
const Moment = require('moment');
const MomentRange = require('moment-range');
const indirectObject = require('@xl/indirect-object-handler');
const validator = require('@xl/input-validator');

const fs = require('fs');
const AWS = require('aws-sdk');

const feCipher = new aes256();
feCipher.createCipher(config.pin.frontend);
const beCipher = new aes256();
beCipher.createCipher(config.pin.backend);

const acceptedErrorStatus = '404';

function doGetProfileKtp(req, res) {
  const fallbackOptions = {};
  const fallback = (error, args) => {
    return Promise.reject({
      message: error.message,
      args
    });
  };
  const { user } = req;
  const authHeader = req.headers.authorization;
  const lang = req.headers.language;
  const requestOptions = jClone.clone(config.apis['selfgetuserprofile']);
  requestOptions.headers.event_id = req.logId;
  requestOptions.headers.actor = user.msisdn;
  const querys = [];
  querys.push(['authHeader', authHeader]);
  return hystrixutil.request('selfgetuserprofile', requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
    if (result && result.data && result.data.result && result.data.result.data) {
      res.sendOk({ errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data: result.data.result.data });
    } else {
      res.sendBusinessError({ errorCode: rc.codes['06'].rc, errorMessage: rc.i18n('06', lang).rm });
    }
  }).catch(error => {
    if (error.message.indexOf(acceptedErrorStatus)) {
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Retrieving Get Profile KTP', message: 'Retrieving Get Profile KTP failed' }));
      res.sendBusinessError({ errorCode: rc.codes['06'].rc, errorMessage: rc.i18n('06', lang).rm });
    } else {
      hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Request Get Profile KTP', message: 'Error Get Profile KTP', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
      res.sendError({ errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm });
    }
  });
}

function doUpdateKtp(req, res) {
  const fallbackOptions = {};
  const fallback = (error, args) => {
    return Promise.reject({
      message: error.message,
      args
    });
  };
  hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Request', message: 'Request Update KTP', data: req.body }));
  const { user } = req;
  const authHeader = req.headers.authorization;
  const lang = req.headers.language;
  user.dealer_id = indirectObject.decrypt(beCipher, user.dealer_id, authHeader);
  const { pin, nationalId, name, birthDate, gender, address, province, district, subdistrict, userConsent } = req.body;

  if (validator.notNullValidator(pin) && validator.notNullValidator(nationalId) && validator.notNullValidator(name) && nameValidator(name)
    && validator.notNullValidator(birthDate) && validator.notNullValidator(gender) && validator.notNullValidator(address)
    && validator.notNullValidator(province) && validator.notNullValidator(district) && validator.notNullValidator(subdistrict)
    && validator.notNullValidator(userConsent) && validator.notNullValidator(user.dealer_id)) {

    const imageFiles = req.files;
    if (imageFiles && imageFiles.length > 0) {
      validatePIN(req).then(result => {
        if (result && result.errorCode === '00') {
          validateKTP(req).then(result => {
            if ((result && result.errorCode && result.errorCode === "00")) {
              const requestOptions = jClone.clone(config.apis['selfupdatektp']);
              requestOptions.headers.event_id = req.logId;
              requestOptions.headers.actor = user.msisdn;
              const querys = [];
              querys.push(['authHeader', authHeader]);
              querys.push(['nationalId', nationalId]);
              querys.push(['name', name]);
              querys.push(['birthDate', birthDate]);
              querys.push(['gender', gender]);
              querys.push(['address', address]);
              querys.push(['province', province]);
              querys.push(['district', district]);
              querys.push(['subdistrict', subdistrict]);
              querys.push(['userConsent', userConsent]);
              querys.push(['dealerId', user.dealer_id]);

              return hystrixutil.request('selfupdatektp', requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
                if (result && result.data && result.data.result && result.data.result.errorCode && result.data.result.errorCode === '00') {
                  doUpdloadKtp(req, res);
                } else {
                  res.sendBusinessError({ errorCode: rc.codes['07'].rc, errorMessage: rc.i18n('07', lang).rm });
                }
              }).catch(error => {
                if (error.message.indexOf(acceptedErrorStatus)) {
                  hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Retrieving Update KTP', message: 'Retrieving Update KTP failed' }));
                  res.sendBusinessError({ errorCode: rc.codes['07'].rc, errorMessage: rc.i18n('07', lang).rm });
                } else {
                  hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Request Update KTP', message: 'Error Update KTP', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
                  res.sendError({ errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm });
                }
              });
            } else {
              hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'KTP validation not valid', data: result.errorCode }));
              res.sendBusinessError({ errorCode: rc.codes[result.errorCode].rc, errorMessage: rc.i18n(result.errorCode, lang).rm });
            }
          });
        } else if (result && result.errorCode === '11') {
          res.sendBusinessError({ errorCode: rc.codes['11'].rc, errorMessage: rc.i18n('11', lang).rm });
        } else {
          res.sendError({ errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm });
        }
      });
    } else {
      res.sendBusinessError({ errorCode: rc.codes['09'].rc, errorMessage: rc.i18n('09', lang).rm });
    }
  } else {
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Check parameter', message: 'Insufficient request parameter' }));
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Insufficient request parameter' }));
    res.sendBusinessError({ errorCode: rc.codes['13'].rc, errorMessage: rc.i18n('13', lang).rm });
  }
}

function doGetProfileKtpRoMini(req, res) {
  const fallbackOptions = {};
  const fallback = (error, args) => {
    return Promise.reject({
      message: error.message,
      args
    });
  };
  const { user } = req;
  const authHeader = req.headers.authorization;
  const lang = req.headers.language;
  const requestOptions = jClone.clone(config.apis['selfgetuserprofile']);
  requestOptions.headers.event_id = req.logId;
  requestOptions.headers.actor = user.msisdn;
  const querys = [];
  querys.push(['authHeader', authHeader]);
  return hystrixutil.request('selfgetuserprofile', requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
    if (result && result.data && result.data.result && result.data.result.data) {
      res.sendOk({ errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data: result.data.result.data });
    } else {
      res.sendBusinessError({ errorCode: rc.codes['06'].rc, errorMessage: rc.i18n('06', lang).rm });
    }
  }).catch(error => {
    if (error.message.indexOf(acceptedErrorStatus)) {
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Retrieving Get Profile KTP', message: 'Retrieving Get Profile KTP failed' }));
      res.sendBusinessError({ errorCode: rc.codes['06'].rc, errorMessage: rc.i18n('06', lang).rm });
    } else {
      hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Request Get Profile KTP', message: 'Error Get Profile KTP', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
      res.sendError({ errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm });
    }
  });
}

function doUpdateKtpRoMini(req, res) {
  const fallbackOptions = {};
  const fallback = (error, args) => {
    return Promise.reject({
      message: error.message,
      args
    });
  };
  hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Request', message: 'Request Update KTP', data: req.body }));
  const { user } = req;
  const authHeader = req.headers.authorization;
  const lang = req.headers.language;
  user.dealer_id = indirectObject.decrypt(beCipher, user.dealer_id, authHeader);
  const { pin, nationalId, name, birthDate, gender, address, province, district, subdistrict, userConsent } = req.body;

  if (validator.notNullValidator(pin) && validator.notNullValidator(nationalId) && validator.notNullValidator(name) && nameValidator(name)
    && validator.notNullValidator(birthDate) && validator.notNullValidator(gender) && validator.notNullValidator(address)
    && validator.notNullValidator(province) && validator.notNullValidator(district) && validator.notNullValidator(subdistrict)
    && validator.notNullValidator(userConsent) && validator.notNullValidator(user.dealer_id)) {

    const imageFiles = req.files;
    if (imageFiles && imageFiles.length > 0) {
      validatePINRoMini(req).then(result => {
        if (result && result.errorCode === '00') {
          validateKTP(req).then(result => {
            if ((result && result.errorCode && result.errorCode === "00")) {
              const requestOptions = jClone.clone(config.apis['selfupdatektp']);
              requestOptions.headers.event_id = req.logId;
              requestOptions.headers.actor = user.msisdn;
              const querys = [];
              querys.push(['authHeader', authHeader]);
              querys.push(['nationalId', nationalId]);
              querys.push(['name', name]);
              querys.push(['birthDate', birthDate]);
              querys.push(['gender', gender]);
              querys.push(['address', address]);
              querys.push(['province', province]);
              querys.push(['district', district]);
              querys.push(['subdistrict', subdistrict]);
              querys.push(['userConsent', userConsent]);
              querys.push(['dealerId', user.dealer_id]);

              return hystrixutil.request('selfupdatektp', requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
                if (result && result.data && result.data.result && result.data.result.errorCode && result.data.result.errorCode === '00') {
                  doUpdloadKtp(req, res);
                } else {
                  res.sendBusinessError({ errorCode: rc.codes['07'].rc, errorMessage: rc.i18n('07', lang).rm });
                }
              }).catch(error => {
                if (error.message.indexOf(acceptedErrorStatus)) {
                  hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Retrieving Update KTP', message: 'Retrieving Update KTP failed' }));
                  res.sendBusinessError({ errorCode: rc.codes['07'].rc, errorMessage: rc.i18n('07', lang).rm });
                } else {
                  hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Request Update KTP', message: 'Error Update KTP', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
                  res.sendError({ errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm });
                }
              });
            } else {
              hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'KTP validation not valid', data: result.errorCode }));
              res.sendBusinessError({ errorCode: rc.codes[result.errorCode].rc, errorMessage: rc.i18n(result.errorCode, lang).rm });
            }
          });
        } else if (result && result.errorCode === '11') {
          res.sendBusinessError({ errorCode: rc.codes['11'].rc, errorMessage: rc.i18n('11', lang).rm });
        } else {
          res.sendError({ errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm });
        }
      });
    } else {
      res.sendBusinessError({ errorCode: rc.codes['09'].rc, errorMessage: rc.i18n('09', lang).rm });
    }
  } else {
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Check parameter', message: 'Insufficient request parameter' }));
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Insufficient request parameter' }));
    res.sendBusinessError({ errorCode: rc.codes['13'].rc, errorMessage: rc.i18n('13', lang).rm });
  }
}

function validateKTP(req) {
  const fallbackOptions = {};
  const fallback = (error, args) => {
    return Promise.reject({
      message: error.message,
      args
    });
  };
  const { user } = req;
  const authHeader = req.headers.authorization;
  const lang = req.headers.language;
  const { nationalId, name, birthDate, gender, address, province, district, subdistrict } = req.body;

  const requestOptions = jClone.clone(config.apis['selfgetlocationid']);
  requestOptions.headers.event_id = req.logId;
  requestOptions.headers.actor = user.msisdn;
  const querys = [];
  querys.push(['authHeader', authHeader]);
  querys.push(['province', province]);
  querys.push(['district', district]);
  querys.push(['subdistrict', subdistrict]);

  return hystrixutil.request('selfgetlocationid', requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
    if (result && result.data && result.data.result && result.data.result.errorCode && result.data.result.errorCode === '00'
      && result.data.result.data && result.data.result.data.location_id) {
      const locationId = result.data.result.data.location_id.replace(/[.]/g, '');
      if (nationalId.substring(0, 6) !== locationId) {
        return { errorCode: "02", data: "National ID invalid" };
      }
      if (gender === 'F') {
        const date = nationalId.substring(6, 12);
        const sub = date.substring(0, 1);
        const parse = parseInt(sub, 10) - 4;
        const newDate = date.replace(date.substring(0, 1), parse);
        if (birthDate !== formatDateKtp(newDate)) {
          return { errorCode: "03", data: "Birthdate invalid " };
        }
      } else if (birthDate !== formatDateKtp(nationalId.substring(6, 12))) {
        return { errorCode: "03", data: "Birthdate invalid " };
      }
      return { errorCode: '00', data: locationId };
    } else {
      return { errorCode: "01", data: "Location ID is not found" };
    }
  }).catch(error => {
    if (error.message.indexOf(acceptedErrorStatus)) {
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Retrieving Validate KTP', message: 'Retrieving Validate KTP failed' }));
      return { errorCode: "01", data: "Location ID is not found" };
    } else {
      hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Request Validate KTP', message: 'Error Validate KTP', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
      return { errorCode: "04", data: "ERROR" };
    }
  });
}

function formatDateKtp(date) {
  const moment = MomentRange.extendMoment(Moment);
  moment.locale('en');
  return moment(date, 'DDMMYY', true).format('YYYY-MM-DD');
}

function doUpdloadKtp(req, res) {
  const lang = req.headers.language;
  const imageFiles = req.files;
  let allowedType = true;
  if (imageFiles && imageFiles.length > 0) {
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Request', message: 'Request Upload KTP', data: imageFiles }));
    const promises = [];
    for (let i = 0; i < imageFiles.length; i++) {
      const file = imageFiles[i];
      if (file.mimetype === 'application/pdf')
        promises.push(uploadToS3Bucket(file, 'KTP/' + file.originalname));
      else
        allowedType = false;
    }
    if (allowedType) {
      Promise.all(promises).then(result => {
        res.sendOk({ errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm });
      }).catch(error => {
        hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Get upload KTP', message: 'Error upload KTP', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
        hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Reach API failed' }));
        res.sendError({ errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm });
      });
    } else {
      res.sendBusinessError({ errorCode: rc.codes['08'].rc, errorMessage: rc.i18n('08', lang).rm });
    }
  } else {
    res.sendBusinessError({ errorCode: rc.codes['09'].rc, errorMessage: rc.i18n('09', lang).rm });
  }
}

function uploadToS3Bucket(imageFile, fileName) {
  AWS.config.update({ accessKeyId: config.S3.accessKeyId, secretAccessKey: config.S3.secretAccessKey, Bucket: config.S3.bucket });
  const { path } = imageFile;
  const s3 = new AWS.S3();
  const params = {
    Bucket: config.S3.bucket,
    Key: fileName,
    ContentType: imageFile.mimetype,
    Body: fs.createReadStream(path)
  }
  return s3.putObject(params).promise();
}

function validatePIN(req) {
  const authHeader = req.headers.authorization;
  const { user } = req;
  const fallbackOptions = {};
  const fallback = (error, args) => {
    return Promise.reject({
      message: error.message,
      args
    });
  };
  const querys = [];
  querys.push(['authHeader', authHeader]);
  querys.push(['pin', encodeURIComponent(req.body.pin)]);
  const requestOptions = jClone.clone(config.apis['selfvalidatepin']);
  requestOptions.headers.event_id = req.logId;
  requestOptions.headers.actor = user.msisdn;
  return hystrixutil.request('selfvalidatepin', requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
    if (result && result.data && result.data.statusCode === 200 && result.data.result && result.data.result.errorCode === '00') {
      return { errorCode: '00', data: [] };
    } else {
      return { errorCode: '11', data: [] };
    }
  }).catch(error => {
    hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Retrieving AWG profile', message: 'Error retrieving AWG profile', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Error retrieving AWG profile' }));
    return { errorCode: '99', data: [] };
  });
}

function validatePINRoMini(req) {
  const authHeader = req.headers.authorization;
  const { user } = req;
  const fallbackOptions = {};
  const fallback = (error, args) => {
    return Promise.reject({
      message: error.message,
      args
    });
  };
  const querys = [];
  querys.push(['authHeader', authHeader]);
  querys.push(['pin', req.body.pin]);
  const requestOptions = jClone.clone(config.apis['selfvalidatepinromini']);
  requestOptions.headers.event_id = req.logId;
  requestOptions.headers.actor = user.msisdn;
  return hystrixutil.request('selfvalidatepinromini', requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
    if (result && result.data && result.data.statusCode === 200 && result.data.result && result.data.result.errorCode === '00') {
      return { errorCode: '00', data: [] };
    } else {
      return { errorCode: '11', data: [] };
    }
  }).catch(error => {
    hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Retrieving RO Mini profile', message: 'Error retrieving RO Mini profile', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Error retrieving RO Mini profile' }));
    return { errorCode: '99', data: [] };
  });
}

function nameValidator(str) {
  const substrings = ['<', '>', '"', '{', '}', '(', ')', '&'];
  if (substrings.some(v => str.includes(v))) {
    return false;
  } else {
    return true;
  }
}

module.exports = {
  getProfileKtp: doGetProfileKtp,
  updateKtp: doUpdateKtp,
  getProfileKtpRoMini: doGetProfileKtpRoMini,
  updateKtpRoMini: doUpdateKtpRoMini,
  getCacheToggle: (req, res) => {
    return req && req.user && req.user.msisdn;
  },
  cacheOptions: {
    appendKey: (req, res) => {
      return req.user.msisdn;
    },
    headers: {
      'cache-control': 'no-cache, no-store, must-revalidate',
      'pragma': 'no-cache',
      'expires': 0
    }
  },
  getCacheDurationInMillis: (req, res) => {
    const today = new Date();
    const seconds = today.getSeconds();
    const minutesToSeconds = today.getMinutes() * 60;
    const hoursToSeconds = today.getHours() * 60 * 60;
    const result = hoursToSeconds + minutesToSeconds + seconds;

    return (86400 - result) * 1000;
  }
}
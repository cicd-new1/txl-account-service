const hydraExpress = require('hydra-express');
const stringify = require('jsesc');
const hystrixutil = require('@xl/hystrix-util');
const config = require('fwsp-config').getObject();
const uuidv1 = require('uuid/v1');
const masheryhttp = require('@xl/mashery-http');
const Aes256 = require('@xl/custom-aes256');
const jClone = require('@xl/json-clone');
const dateutil = require('@xl/util-date');
const rc = require('../constant/RC');
const apigatehttp = require('@xl/apigate-http');

const feCipher = new Aes256();
feCipher.createCipher(config.pin.frontend);
const beCipher = new Aes256();
beCipher.createCipher(config.pin.backend);

module.exports = {
  generateOtp : (req, res) => {
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Request', message: 'Request generate OTP for change pin'}));
    const { user } = req;
    const fallbackOptions = {};
    const fallback = (error, args) => {
      return Promise.reject({
        message: error.message,
        args
      });
    };
    const lang = req.headers.language;
    const requestId = uuidv1();
    const requestDate = dateutil.formatCommonDateTime(new Date());
    const otpRequestOptions = jClone.clone(config.apis['generateotp']);
    otpRequestOptions.headers.event_id = req.logId;
    otpRequestOptions.headers.actor = user.msisdn;
    const otpQuerys = [];
    otpQuerys.push(['requestId', requestId]);
    otpQuerys.push(['requestDate', requestDate]);
    otpQuerys.push(['msisdn', user.msisdn]);
    otpQuerys.push(['channel', 'TOKOXLAPPCHANGEPIN']);
    hystrixutil.request('generateotp', otpRequestOptions, masheryhttp.request, fallbackOptions, fallback, otpQuerys).then(otpResult => {
      const responseCode = otpResult && otpResult.data && otpResult.data.opSendOtpRs ? otpResult.data.opSendOtpRs.headerRs.responseCode : '01';
      if(responseCode === '00') {
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Generate OTP for change pin', message: 'Generate OTP for change pin success'}));
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'OTP for change pin sent'}));
        res.sendOk({errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm});
      } else {
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Generate OTP for change pin', message: 'Generate OTP for change pin failed', data: otpResult.data}));
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'OTP for change pin failed'}));
        res.sendError({errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm});
      }
    }).catch(error => {
      hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Generate OTP for change pin', message: 'Generate OTP for change pin failed', error: '['+ error.message.status +'] '+ JSON.stringify(error.message.data), stack: error.stack}));
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Reaching API failed'}));
      res.sendError({errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm});
    });
  },
  generateOtpV2 : (req, res) => {
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Request', message: 'Request generate OTP for change pin'}));
    const { user } = req;
    const fallbackOptions = {};
    const fallback = (error, args) => {
      return Promise.reject({
        message: error.message,
        args
      });
    };
    const lang = req.headers.language;
    const otpRequestOptions = jClone.clone(config.apis['generateotpV2']);
    otpRequestOptions.headers.event_id = req.logId;
    otpRequestOptions.headers.actor = user.msisdn;
    const otpQuerys = [];
    otpQuerys.push(['msisdn', user.msisdn]);
    otpQuerys.push(['channel', 'TOKOXLAPPCHANGEPIN']);
    hystrixutil.request('generateotpV2', otpRequestOptions, apigatehttp.request, fallbackOptions, fallback, otpQuerys).then(otpResult => {
      const responseCode = otpResult && otpResult.data && otpResult.data.reason;
      if(responseCode === 'Success') {
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Generate OTP for change pin', message: 'Generate OTP for change pin success'}));
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'OTP for change pin sent'}));
        res.sendOk({errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm});
      } else {
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Generate OTP for change pin', message: 'Generate OTP for change pin failed', data: otpResult.data}));
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'OTP for change pin failed'}));
        res.sendError({errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm});
      }
    }).catch(error => {
      hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Generate OTP for change pin', message: 'Generate OTP for change pin failed', error: '['+ error.message.status +'] '+ JSON.stringify(error.message.data), stack: error.stack}));
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Reaching API failed'}));
      res.sendError({errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm});
    });
  },
  changePin: (req, res) => {
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Request', message: 'Request change pin'}));
    const lang = req.headers.language;
    const { user } = req;
    const fallbackOptions = {};
    const fallback = (error, args) => {
      return Promise.reject({
        message: error.message,
        args
      });
    };
    module.exports.validateOTPV2(req, 'TOKOXLAPPCHANGEPIN').then(result => {
      if(result) {
        try {
          let oldPin = feCipher.decrypt(req.body.oldPin);
          let newPin = feCipher.decrypt(req.body.newPin);
          oldPin = beCipher.encrypt(oldPin);
          newPin = beCipher.encrypt(newPin);
          const requestOptions = jClone.clone(config.apis['rochangepin']);
          requestOptions.headers.event_id = req.logId;
          requestOptions.headers.actor = user.msisdn;
          const querys = [];
          querys.push(['msisdn', user.msisdn]);
          querys.push(['oldPin', oldPin]);
          querys.push(['newPin', newPin]);
          hystrixutil.request('rochangepin', requestOptions, apigatehttp.request, fallbackOptions, fallback, querys).then(result => {
            if(result && result.data && result.data.data) {
              hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Change pin', message: 'Change pin success'}));
              res.sendOk({errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm});
            } else {
              hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Change pin', message: 'Change pin failed', data: result.data}));
              hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'PIN not match'}));
              res.sendBusinessError({errorCode: rc.codes['11'].rc, errorMessage: rc.i18n('11', lang).rm});
            }
          }).catch(error => {
            if(error.message.status === 401) {
              hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Change pin', message: 'PIN not match', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
              hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'PIN not match'}));
              res.sendBusinessError({errorCode: rc.codes['11'].rc, errorMessage: rc.i18n('11', lang).rm});
            } else {
              hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Change pin', message: 'Error changing pin', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
              hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Reach API failed'}));
              res.sendError({errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm});
            }
          });
        } catch(error) {
          hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Change pin', message: 'PIN not match', error: error.message, stack: error.stack}));
          hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'PIN not match'}));
          res.sendBusinessError({errorCode: rc.codes['11'].rc, errorMessage: rc.i18n('11', lang).rm});
        }
      } else {
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Validate SMS OTP', message: 'Validate SMS OTP failed'}));
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Validate SMS OTP failed'}));
        res.sendBusinessError({errorCode: rc.codes['15'].rc, errorMessage: rc.i18n('15', lang).rm});
      }
    });
  },
  validateOTP: (req, channel) => {
    const fallbackOptions = {};
    const fallback = (error, args) => {
      return Promise.reject({
        message: error.message,
        args
      });
    };
    const { user } = req;
    const { otp } = req.body;
    let bypassStatus = false;
    if(config.otpWhitelist) {
      for(let i = 0; i < config.otpWhitelist.length; i++) {
        if(config.otpWhitelist[i].msisdn === user.msisdn && config.otpWhitelist[i].otp === otp) {
          bypassStatus = true;
          break;
        }
      }
    }
    if(bypassStatus) {
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Validate SMS OTP', message: 'Validate SMS OTP success'}));
      return true;
    } else {
      const requestOptions = jClone.clone(config.apis['validateotp']);
      requestOptions.headers.event_id = req.logId;
      requestOptions.headers.actor = user.msisdn;
      const otpQuerys = [];
      otpQuerys.push(['msisdn', user.msisdn]);
      otpQuerys.push(['otp', otp]);
      otpQuerys.push(['channel', channel]);
      return hystrixutil.request('validateotp', requestOptions, masheryhttp.request, fallbackOptions, fallback, otpQuerys).then(otpResult => {
        if(otpResult && otpResult.data && otpResult.data.headerRs && otpResult.data.headerRs.responseCode && otpResult.data.headerRs.responseCode === '00') {
          hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Validate SMS OTP', message: 'Validate SMS OTP success'}));
          return true;
        } else {
          hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Validate SMS OTP', message: 'Validate SMS OTP failed'}));
          return false;
        }
      }).catch(error => {
        hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Validate SMS OTP', message: 'Validate SMS OTP failed', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
        hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Validate SMS OTP', message: 'Error validating SMS OTP', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
        return false;
      });
    }
  },
  validateOTPV2: (req, channel) => {
    const fallbackOptions = {};
    const fallback = (error, args) => {
      return Promise.reject({
        message: error.message,
        args
      });
    };
    const { user } = req;
    const { otp } = req.body;
    let bypassStatus = false;
    if(config.otpWhitelist) {
      for(let i = 0; i < config.otpWhitelist.length; i++) {
        if(config.otpWhitelist[i].msisdn === user.msisdn && config.otpWhitelist[i].otp === otp) {
          bypassStatus = true;
          break;
        }
      }
    }
    if(bypassStatus) {
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Validate SMS OTP', message: 'Validate SMS OTP success'}));
      return true;
    } else {
      const requestOptions = jClone.clone(config.apis['validateotpV2']);
      requestOptions.headers.event_id = req.logId;
      requestOptions.headers.actor = user.msisdn;
      const otpQuerys = [];
      otpQuerys.push(['msisdn', user.msisdn]);
      otpQuerys.push(['otp', otp]);
      otpQuerys.push(['channel', channel]);
      return hystrixutil.request('validateotpV2', requestOptions, apigatehttp.request, fallbackOptions, fallback, otpQuerys).then(otpResult => {
        const responseCode = otpResult && otpResult.data && otpResult.data.reason;
        if(responseCode === 'OK') {          
          hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Validate SMS OTP', message: 'Validate SMS OTP success'}));
          return true;
        } else {
          hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Validate SMS OTP', message: 'Validate SMS OTP failed'}));
          return false;
        }
      }).catch(error => {
        hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Validate SMS OTP', message: 'Validate SMS OTP failed', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
        hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Validate SMS OTP', message: 'Error validating SMS OTP', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
        return false;
      });
    }
  }
}
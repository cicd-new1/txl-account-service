const hydraExpress = require('hydra-express');
const stringify = require('jsesc');
const hystrixutil = require('@xl/hystrix-util');
const config = require('fwsp-config').getObject();
const jClone = require('@xl/json-clone');
const rc = require('../constant/RC');
const axios = require('axios');
const cfenv = require('cfenv');
const intercomm = require('@xl/cf-intercomm');


const acceptedErrorStatus = '404';

function doGetProfileByEmail(req, res) {
  const lang = req.headers.language;
  const { user } = req;
  hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Request', message: 'Request for get ro mini profile by email ' + user.email + ' (' + lang + ')' }));

  const fallbackOptions = {};
  const requestOptions = jClone.clone(config.apis['selfgetrominiuserbyemail']);
  requestOptions.headers.event_id = req.logId;
  requestOptions.headers.actor = user.msisdn;
  const query = [];
  query.push(['email', user.email]);
  query.push(['authorization', req.headers.authorization]);
  hystrixutil.request('selfgetrominiuserbyemail', requestOptions, axios, fallbackOptions, fallback, query).then(result => {
    if (result && result.data && result.data.statusCode && result.data.statusCode === 200
      && result.data.result && result.data.result.data && result.data.result.data.profile && result.data.result.data.profile.ro_id) {
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Retrieving RO Mini profile', message: 'Retrieving RO Mini profile success' }));
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Authorization response' }));
      res.sendOk({ errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data: result.data.result.data.profile });
    } else {
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Reach API failed' }));
      res.sendError({ errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm });
    }
  }).catch(error => {
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Reach API failed' }));
    res.sendError({ errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm });
  });
}

function doSendEmailOTP(req, res) {
  const fallbackOptions = {};
  const fallback = (error, args) => {
    return Promise.reject({
      message: error.message,
      args
    });
  };
  const lang = req.headers.language;
  const { user } = req;
  const otpRequestOptions = jClone.clone(config.apis['selfsendemailotp']);
  otpRequestOptions.headers.event_id = req.logId;
  otpRequestOptions.headers.actor = user.msisdn;
  const otpQuerys = [];
  otpQuerys.push(['email', user.email]);
  otpQuerys.push(['authorization', req.headers.authorization]);
  hystrixutil.request('selfsendemailotp', otpRequestOptions, axios, fallbackOptions, fallback, otpQuerys).then(otpResult => {
    const responseCode = otpResult && otpResult.data && otpResult.data.statusCode;
    if (responseCode === 200) {
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Send Email OTP', message: 'Send Email OTP success' }));
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Email OTP sent' }));
      res.sendOk({ errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm });
    } else {
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Send Email OTP', message: 'Send Email OTP failed', data: otpResult.data }));
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Email OTP failed' }));
      res.sendError({ errorCode: rc.codes['90'].rc, errorMessage: rc.i18n('90', lang).rm });
    }
  }).catch(function (error) {
    if (error.message.indexOf(acceptedErrorStatus)) {
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Send Email OTP', message: 'Send Email OTP failed', data: otpResult.data }));
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Email OTP failed' }));
      res.sendError({ errorCode: rc.codes['90'].rc, errorMessage: rc.i18n('90', lang).rm });
    } else {
      hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Send Email OTP', message: 'Send Email OTP failed', error: '[' + error.message + '] ' + JSON.stringify(error.message), stack: error.stack }));
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Reaching API failed' }));
      res.sendError({ errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm });
    }
  });
}

function doResetPin(req, res) {
  hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Request', message: 'Request reset pin' }));
  const lang = req.headers.language;
  const { user } = req;
  const { email, otp } = req.body;
  const fallbackOptions = {};
  const fallback = (error, args) => {
    return Promise.reject({
      message: error.message,
      args
    });
  };
  const requestOptions = jClone.clone(config.apis['selfvalidateemailotp']);
  requestOptions.headers.event_id = req.logId;
  requestOptions.headers.actor = user.msisdn;
  const otpQuerys = [];
  otpQuerys.push(['email', email]);
  otpQuerys.push(['otpCode', otp]);
  otpQuerys.push(['authorization', req.headers.authorization]);
  hystrixutil.request('selfvalidateemailotp', requestOptions, axios, fallbackOptions, fallback, otpQuerys).then(otpResult => {
    const responseCode = otpResult && otpResult.data && otpResult.data.statusCode;
    if (responseCode === 200) {
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Validate Email OTP', message: 'Validate Email OTP success' }));
      resetPIN(req, res);
    } else {
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Validate Email OTP', message: 'Validate Email OTP failed' }));
      res.sendBusinessError({ errorCode: rc.codes['15'].rc, errorMessage: rc.i18n('15', lang).rm });
    }
  }).catch(function (error) {
    if (error.message.indexOf(acceptedErrorStatus)) {
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Validate Email OTP', message: 'Validate Email OTP failed' }));
      res.sendBusinessError({ errorCode: rc.codes['15'].rc, errorMessage: rc.i18n('15', lang).rm });
    } else {
      hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Validate Email OTP', message: 'Validate Email OTP failed', error: '[' + error.message + '] ' + stringify(error.message), stack: error.stack }));
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Reaching API failed' }));
      res.sendError({ errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm });
    }
  });
}

function doChangePin(req, res) {
  hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Request', message: 'Request change pin' }));
  const lang = req.headers.language;
  const { user } = req;
  const { email, otp } = req.body;
  const fallbackOptions = {};
  const fallback = (error, args) => {
    return Promise.reject({
      message: error.message,
      args
    });
  };
  const requestOptions = jClone.clone(config.apis['selfvalidateemailotp']);
  requestOptions.headers.event_id = req.logId;
  requestOptions.headers.actor = user.msisdn;
  const otpQuerys = [];
  otpQuerys.push(['email', email]);
  otpQuerys.push(['otpCode', otp]);
  otpQuerys.push(['authorization', req.headers.authorization]);
  hystrixutil.request('selfvalidateemailotp', requestOptions, axios, fallbackOptions, fallback, otpQuerys).then(otpResult => {
    const responseCode = otpResult && otpResult.data && otpResult.data.statusCode;
    if (responseCode === 200) {
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Validate Email OTP', message: 'Validate Email OTP success' }));
      changePIN(req, res);
    } else {
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Validate Email OTP', message: 'Validate Email OTP failed' }));
      res.sendBusinessError({ errorCode: rc.codes['15'].rc, errorMessage: rc.i18n('15', lang).rm });
    }
  }).catch(function (error) {
    if (error.message.indexOf(acceptedErrorStatus)) {
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Validate Email OTP', message: 'Validate Email OTP failed' }));
      res.sendBusinessError({ errorCode: rc.codes['15'].rc, errorMessage: rc.i18n('15', lang).rm });
    } else {
      hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Validate Email OTP', message: 'Validate Email OTP failed', error: '[' + error.message + '] ' + stringify(error.message), stack: error.stack }));
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Reaching API failed' }));
      res.sendError({ errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm });
    }
  });
}

function resetPIN(req, res) {
  const lang = req.headers.language;
  const { user } = req;
  const fallbackOptions = {};
  const fallback = (error, args) => {
    return Promise.reject({
      message: error.message,
      args
    });
  };
  const requestOptions = jClone.clone(config.apis['selfrominiresetpin']);
  requestOptions.headers.event_id = req.logId;
  requestOptions.headers.actor = user.msisdn;
  const query = [];
  query.push(['authorization', req.headers.authorization]);
  hystrixutil.request('selfrominiresetpin', requestOptions, axios, fallbackOptions, fallback, query).then(otpResult => {
    const responseCode = otpResult && otpResult.data && otpResult.data.statusCode;
    if (responseCode === 200) {
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Reset PIN RO Mini', message: 'Reset PIN RO Mini success' }));
      res.sendOk({ errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm });
    } else {
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Reset PIN RO Mini', message: 'Reset PIN RO Mini failed' }));
      res.sendBusinessError({ errorCode: rc.codes['26'].rc, errorMessage: rc.i18n('26', lang).rm });
    }
  }).catch(function (error) {
    if (error.message.indexOf(acceptedErrorStatus)) {
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Reset PIN RO Mini', message: 'Reset PIN RO Mini failed' }));
      res.sendBusinessError({ errorCode: rc.codes['26'].rc, errorMessage: rc.i18n('26', lang).rm });
    } else {
      hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Reset PIN RO Mini', message: 'Reset PIN RO Mini failed', error: '[' + error.message + '] ' + stringify(error.message), stack: error.stack }));
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Reaching API failed' }));
      res.sendError({ errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm });
    }
  });
}

function changePIN(req, res) {
  const lang = req.headers.language;
  const { user } = req;
  const { oldPin, newPin } = req.body;
  const fallbackOptions = {};
  const fallback = (error, args) => {
    return Promise.reject({
      message: error.message,
      args
    });
  };
  const requestOptions = jClone.clone(config.apis['selfrominichangepin']);
  requestOptions.headers.event_id = req.logId;
  requestOptions.headers.actor = user.msisdn;
  const otpQuerys = [];
  otpQuerys.push(['oldPin', oldPin]);
  otpQuerys.push(['newPin', newPin]);
  otpQuerys.push(['authorization', req.headers.authorization]);
  hystrixutil.request('selfrominichangepin', requestOptions, axios, fallbackOptions, fallback, otpQuerys).then(otpResult => {
    const responseCode = otpResult && otpResult.data && otpResult.data.statusCode;
    if (responseCode === 200) {
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Change PIN RO Mini', message: 'Change PIN RO Mini success' }));
      res.sendOk({ errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm });
    } else {
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Change PIN RO Mini', message: 'Change PIN RO Mini failed' }));
      res.sendBusinessError({ errorCode: rc.codes['27'].rc, errorMessage: rc.i18n('27', lang).rm });
    }
  }).catch(function (error) {
    if (error.message.indexOf(acceptedErrorStatus)) {
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Change PIN RO Mini', message: 'Change PIN RO Mini failed' }));
      res.sendBusinessError({ errorCode: rc.codes['27'].rc, errorMessage: rc.i18n('27', lang).rm });
    } else {
      hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Change PIN RO Mini', message: 'Change PIN RO Mini failed', error: '[' + error.message + '] ' + stringify(error.message), stack: error.stack }));
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Reaching API failed' }));
      res.sendError({ errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm });
    }
  });
}

function doValidatePin(req, res) {
  hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Request', message: 'Request validate pin' }));
  const lang = req.headers.language;
  const type = 'validpin';
  intercommThrottle(req, type).then(response => {
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Calling intercomm throttle service', message: 'Throttle service success' }));
    validatePIN(req, res, type);
  }).catch(error => {
    hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Calling intercomm throttle service', message: 'Error calling intercomm throttle service', error: '[' + error.message.status + '] ' + stringify(error.message.data), stack: error.stack }));
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Calling intercomm unthrottle service', message: 'Destroy throttling' }));
    intercommUnthrottle(req, type).then(result => {
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Calling intercomm logout service', message: 'Force logout' }));
      intercommLogout(req);
    });
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Force logout' }));
    res.sendInvalidUserCredentials({ errorCode: rc.codes['14'].rc, errorMessage: rc.i18n('14', lang).rm });
  });
}

function validatePIN(req, res, type) {
  hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Request', message: 'Request validate pin' }));
  const lang = req.headers.language;
  const { user } = req;
  const { pin } = req.body;
  const fallbackOptions = {};
  const fallback = (error, args) => {
    return Promise.reject({
      message: error.message,
      args
    });
  };
  const requestOptions = jClone.clone(config.apis['selfrominivalidatepin']);
  requestOptions.headers.event_id = req.logId;
  requestOptions.headers.actor = user.msisdn;
  const querys = [];
  querys.push(['pin', pin]);
  querys.push(['authorization', req.headers.authorization]);
  hystrixutil.request('selfrominivalidatepin', requestOptions, axios, fallbackOptions, fallback, querys).then(result => {
    const responseCode = result && result.data && result.data.statusCode;
    if (responseCode === 200) {
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Validate PIN', message: 'Validate PIN success' }));
      intercommUnthrottle(req, type);
      res.sendOk({ errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm });
    } else {
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Validate pin', message: 'PIN not match' }));
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'PIN not match' }));
      res.sendOk({ errorCode: rc.codes['11'].rc, errorMessage: rc.i18n('11', lang).rm });
    }
  }).catch(function (error) {
    if (error.message.indexOf(acceptedErrorStatus)) {
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Validate pin', message: 'PIN not match' }));
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'PIN not match' }));
      res.sendOk({ errorCode: rc.codes['11'].rc, errorMessage: rc.i18n('11', lang).rm });
    } else {
      hydraExpress.appLogger.error(stringify({ id: req.logId, event: 'Validate pin', message: 'Error validating pin', error: '[' + error.message + '] ' + stringify(error.message), stack: error.stack }));
      hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Response', message: 'Reach API failed' }));
      res.sendError({ errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm });
    }
  });
}

function intercommThrottle(req, type) {
  const { user } = req;
  const throttleId = user.msisdn;
  const maxRetry = 5;
  const duration = 30;
  const authHeader = req.headers.authorization;
  const envVars = cfenv.getAppEnv();

  const environment = !envVars.isLocal ? (envVars.app.space_name == 'prod' ? '' : '-' + envVars.app.space_name) : (process.env.EKS_ENV && process.env.EKS_ENV !== '' ? process.env.EKS_ENV + '-' : '');
  const apiServiceName = !envVars.isLocal ? 'txl-login-controller-service' + environment : environment + 'txl-login-controller-service';
  const apiNameService = 'throttle';
  const apiQuerys = [['authorizationHeader', authHeader], ['throttleId', throttleId], ['type', type], ['maxretry', maxRetry], ['duration', duration]];

  const apiOptions = {
    throttle: {
      'url': '/v1/login/token/throttle/${throttleId}?type=${type}&duration=${duration}&maxretry=${maxretry}',
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': '${authorizationHeader}'
      },
      circuitBreakerSleepWindowInMilliseconds: 5000,
      circuitBreakerRequestVolumeThreshold: 5,
      circuitBreakerForceOpened: false,
      circuitBreakerForceClosed: false,
      circuitBreakerErrorThresholdPercentage: 60,
      statisticalWindowLength: 10000,
      statisticalWindowNumberOfBuckets: 10,
      requestVolumeRejectionThreshold: 100,
      timeout: 5000
    },
    unthrottle: {
      url: '/v1/login/token/unthrottle/${throttleId}?type=${type}',
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': '${authorizationHeader}'
      },
      circuitBreakerSleepWindowInMilliseconds: 5000,
      circuitBreakerRequestVolumeThreshold: 5,
      circuitBreakerForceOpened: false,
      circuitBreakerForceClosed: false,
      circuitBreakerErrorThresholdPercentage: 60,
      statisticalWindowLength: 10000,
      statisticalWindowNumberOfBuckets: 10,
      requestVolumeRejectionThreshold: 100,
      timeout: 5000
    },
    logout: {
      url: '/v1/auth/logout',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': '${authorizationHeader}'
      },
      circuitBreakerSleepWindowInMilliseconds: 5000,
      circuitBreakerRequestVolumeThreshold: 5,
      circuitBreakerForceOpened: false,
      circuitBreakerForceClosed: false,
      circuitBreakerErrorThresholdPercentage: 60,
      statisticalWindowLength: 10000,
      statisticalWindowNumberOfBuckets: 10,
      requestVolumeRejectionThreshold: 100,
      timeout: 5000
    }
  };

  if (!envVars.isLocal) {
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Check environment', message: 'PCF ' + environment + ' environment' }));
    const options = {
      serviceName: apiServiceName,
      apiName: apiNameService,
      querys: apiQuerys,
      serviceOptions: {
        credentials: {
          url: 'http://txl-login-controller-service' + environment + '.apps.internal:8080'
        },
        apis: apiOptions
      }
    };
    if (options.serviceOptions && options.serviceOptions.apis && options.serviceOptions.apis[apiNameService]) {
      options.serviceOptions.apis[apiNameService].headers.event_id = req.logId;
      options.serviceOptions.apis[apiNameService].headers.actor = user.msisdn;
    }
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Throttling', message: 'Intercomm increment throttle' }));
    return intercomm.request(options);
  } else if (process.env.EKS_ENV) {
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Check environment', message: 'EKS ' + environment + ' environment' }));
    const options = {
      serviceName: apiServiceName,
      apiName: apiNameService,
      querys: apiQuerys,
      serviceOptions: {
        credentials: {
          url: 'http://' + environment + 'txl-login-controller-service:8080'
        },
        apis: apiOptions
      }
    };
    if (options.serviceOptions && options.serviceOptions.apis && options.serviceOptions.apis[apiNameService]) {
      options.serviceOptions.apis[apiNameService].headers.event_id = req.logId;
      options.serviceOptions.apis[apiNameService].headers.actor = user.msisdn;
    }
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Throttling', message: 'Intercomm increment throttle' }));
    return intercomm.request(options);
  } else {
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Check environment', message: 'EKS ' + environment + ' environment' }));
    const options = {
      serviceName: apiServiceName,
      apiName: apiNameService,
      querys: apiQuerys,
      serviceOptions: {
        credentials: {
          url: 'http://localhost:8082'
        },
        apis: apiOptions
      }
    };
    if (options.serviceOptions && options.serviceOptions.apis && options.serviceOptions.apis[apiNameService]) {
      options.serviceOptions.apis[apiNameService].headers.event_id = req.logId;
      options.serviceOptions.apis[apiNameService].headers.actor = user.msisdn;
    }
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Throttling', message: 'Intercomm increment throttle' }));
    return intercomm.request(options);
  }
}

function intercommUnthrottle(req, type) {
  const { user } = req;
  const throttleId = user.msisdn;
  const authHeader = req.headers.authorization;
  const envVars = cfenv.getAppEnv();

  const environment = !envVars.isLocal ? (envVars.app.space_name == 'prod' ? '' : '-' + envVars.app.space_name) : (process.env.EKS_ENV && process.env.EKS_ENV !== '' ? process.env.EKS_ENV + '-' : '');
  const apiServiceName = !envVars.isLocal ? 'txl-login-controller-service' + environment : environment + 'txl-login-controller-service';
  const apiNameService = 'unthrottle';
  const apiQuerys = [['authorizationHeader', authHeader], ['throttleId', throttleId], ['type', type]];

  const apiOptions = {
    throttle: {
      'url': '/v1/login/token/throttle/${throttleId}?type=${type}&duration=${duration}&maxretry=${maxretry}',
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': '${authorizationHeader}'
      },
      circuitBreakerSleepWindowInMilliseconds: 5000,
      circuitBreakerRequestVolumeThreshold: 5,
      circuitBreakerForceOpened: false,
      circuitBreakerForceClosed: false,
      circuitBreakerErrorThresholdPercentage: 60,
      statisticalWindowLength: 10000,
      statisticalWindowNumberOfBuckets: 10,
      requestVolumeRejectionThreshold: 100,
      timeout: 5000
    },
    unthrottle: {
      url: '/v1/login/token/unthrottle/${throttleId}?type=${type}',
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': '${authorizationHeader}'
      },
      circuitBreakerSleepWindowInMilliseconds: 5000,
      circuitBreakerRequestVolumeThreshold: 5,
      circuitBreakerForceOpened: false,
      circuitBreakerForceClosed: false,
      circuitBreakerErrorThresholdPercentage: 60,
      statisticalWindowLength: 10000,
      statisticalWindowNumberOfBuckets: 10,
      requestVolumeRejectionThreshold: 100,
      timeout: 5000
    },
    logout: {
      url: '/v1/auth/logout',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': '${authorizationHeader}'
      },
      circuitBreakerSleepWindowInMilliseconds: 5000,
      circuitBreakerRequestVolumeThreshold: 5,
      circuitBreakerForceOpened: false,
      circuitBreakerForceClosed: false,
      circuitBreakerErrorThresholdPercentage: 60,
      statisticalWindowLength: 10000,
      statisticalWindowNumberOfBuckets: 10,
      requestVolumeRejectionThreshold: 100,
      timeout: 5000
    }
  };

  if (!envVars.isLocal) {
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Check environment', message: 'PCF ' + environment + ' environment' }));
    const options = {
      serviceName: apiServiceName,
      apiName: apiNameService,
      querys: apiQuerys,
      serviceOptions: {
        credentials: {
          url: 'http://txl-login-controller-service' + environment + '.apps.internal:8080'
        },
        apis: apiOptions
      }
    };
    if (options.serviceOptions && options.serviceOptions.apis && options.serviceOptions.apis[apiNameService]) {
      options.serviceOptions.apis[apiNameService].headers.event_id = req.logId;
      options.serviceOptions.apis[apiNameService].headers.actor = user.msisdn;
    }
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Throttling', message: 'Intercomm destroy throttle' }));
    return intercomm.request(options);
  } else if (process.env.EKS_ENV) {
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Check environment', message: 'EKS ' + environment + ' environment' }));
    const options = {
      serviceName: apiServiceName,
      apiName: apiNameService,
      querys: apiQuerys,
      serviceOptions: {
        credentials: {
          url: 'http://' + environment + 'txl-login-controller-service:8080'
        },
        apis: apiOptions
      }
    };
    if (options.serviceOptions && options.serviceOptions.apis && options.serviceOptions.apis[apiNameService]) {
      options.serviceOptions.apis[apiNameService].headers.event_id = req.logId;
      options.serviceOptions.apis[apiNameService].headers.actor = user.msisdn;
    }
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Throttling', message: 'Intercomm destroy throttle' }));
    return intercomm.request(options);
  } else {
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Check environment', message: 'EKS ' + environment + ' environment' }));
    const options = {
      serviceName: apiServiceName,
      apiName: apiNameService,
      querys: apiQuerys,
      serviceOptions: {
        credentials: {
          url: 'http://localhost:8082'
        },
        apis: apiOptions
      }
    };
    if (options.serviceOptions && options.serviceOptions.apis && options.serviceOptions.apis[apiNameService]) {
      options.serviceOptions.apis[apiNameService].headers.event_id = req.logId;
      options.serviceOptions.apis[apiNameService].headers.actor = user.msisdn;
    }
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Throttling', message: 'Intercomm destroy throttle' }));
    return intercomm.request(options);
  }
}

function intercommLogout(req) {
  const { user } = req;
  const authHeader = req.headers.authorization;
  const envVars = cfenv.getAppEnv();

  const environment = !envVars.isLocal ? (envVars.app.space_name == 'prod' ? '' : '-' + envVars.app.space_name) : (process.env.EKS_ENV && process.env.EKS_ENV !== '' ? process.env.EKS_ENV + '-' : '');
  const apiServiceName = !envVars.isLocal ? 'txl-login-controller-service' + environment : environment + 'txl-login-controller-service';
  const apiNameService = 'logout';
  const apiQuerys = [['authorizationHeader', authHeader]];

  const apiOptions = {
    throttle: {
      'url': '/v1/login/token/throttle/${throttleId}?type=${type}&duration=${duration}&maxretry=${maxretry}',
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': '${authorizationHeader}'
      },
      circuitBreakerSleepWindowInMilliseconds: 5000,
      circuitBreakerRequestVolumeThreshold: 5,
      circuitBreakerForceOpened: false,
      circuitBreakerForceClosed: false,
      circuitBreakerErrorThresholdPercentage: 60,
      statisticalWindowLength: 10000,
      statisticalWindowNumberOfBuckets: 10,
      requestVolumeRejectionThreshold: 100,
      timeout: 5000
    },
    unthrottle: {
      url: '/v1/login/token/unthrottle/${throttleId}?type=${type}',
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': '${authorizationHeader}'
      },
      circuitBreakerSleepWindowInMilliseconds: 5000,
      circuitBreakerRequestVolumeThreshold: 5,
      circuitBreakerForceOpened: false,
      circuitBreakerForceClosed: false,
      circuitBreakerErrorThresholdPercentage: 60,
      statisticalWindowLength: 10000,
      statisticalWindowNumberOfBuckets: 10,
      requestVolumeRejectionThreshold: 100,
      timeout: 5000
    },
    logout: {
      url: '/v1/auth/logout',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': '${authorizationHeader}'
      },
      circuitBreakerSleepWindowInMilliseconds: 5000,
      circuitBreakerRequestVolumeThreshold: 5,
      circuitBreakerForceOpened: false,
      circuitBreakerForceClosed: false,
      circuitBreakerErrorThresholdPercentage: 60,
      statisticalWindowLength: 10000,
      statisticalWindowNumberOfBuckets: 10,
      requestVolumeRejectionThreshold: 100,
      timeout: 5000
    }
  };

  if (!envVars.isLocal) {
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Check environment', message: 'PCF ' + environment + ' environment' }));
    const options = {
      serviceName: apiServiceName,
      apiName: apiNameService,
      querys: apiQuerys,
      serviceOptions: {
        credentials: {
          url: 'http://txl-login-controller-service' + environment + '.apps.internal:8080'
        },
        apis: apiOptions
      }
    };
    if (options.serviceOptions && options.serviceOptions.apis && options.serviceOptions.apis[apiNameService]) {
      options.serviceOptions.apis[apiNameService].headers.event_id = req.logId;
      options.serviceOptions.apis[apiNameService].headers.actor = user.msisdn;
    }
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Logout', message: 'Intercomm logout' }));
    return intercomm.request(options);
  } else if (process.env.EKS_ENV) {
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Check environment', message: 'EKS ' + environment + ' environment' }));
    const options = {
      serviceName: apiServiceName,
      apiName: apiNameService,
      querys: apiQuerys,
      serviceOptions: {
        credentials: {
          url: 'http://' + environment + 'txl-login-controller-service:8080'
        },
        apis: apiOptions
      }
    };
    if (options.serviceOptions && options.serviceOptions.apis && options.serviceOptions.apis[apiNameService]) {
      options.serviceOptions.apis[apiNameService].headers.event_id = req.logId;
      options.serviceOptions.apis[apiNameService].headers.actor = user.msisdn;
    }
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Logout', message: 'Intercomm logout' }));
    return intercomm.request(options);
  } else {
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Check environment', message: 'EKS ' + environment + ' environment' }));
    const options = {
      serviceName: apiServiceName,
      apiName: apiNameService,
      querys: apiQuerys,
      serviceOptions: {
        credentials: {
          url: 'http://localhost:8082'
        },
        apis: apiOptions
      }
    };
    if (options.serviceOptions && options.serviceOptions.apis && options.serviceOptions.apis[apiNameService]) {
      options.serviceOptions.apis[apiNameService].headers.event_id = req.logId;
      options.serviceOptions.apis[apiNameService].headers.actor = user.msisdn;
    }
    hydraExpress.appLogger.info(stringify({ id: req.logId, event: 'Logout', message: 'Intercomm logout' }));
    return intercomm.request(options);
  }
}

function fallback(error, args) {
  return Promise.reject({
    message: error.message,
    args
  });
}

module.exports = {
  getRoMiniUserByEmail: doGetProfileByEmail,
  sendOtpEmail: doSendEmailOTP,
  resetPin: doResetPin,
  changePin: doChangePin,
  validatePin: doValidatePin
}
#!/bin/bash

set +x

/usr/bin/npm-cli-adduser --registry https://npmrepos.int.dp.xl.co.id/ --scope @xl --username xl --password Mw7WZM7FLHner3kh8tTynNcWsBNqzHEeK --email rachmasaric@xl.co.id
npm install
npm audit fix
npm run actuator
cf -v
cf api https://api.system.pcf.kb.dp.xl.co.id/ --skip-ssl-validation
cf auth developer password123
cf target -o tokoXL -s staging
APP='txl-account-service-staging'
#cf blue-green-deploy ${APP}

cf push ${APP}-new -n ${APP}
cf delete -f ${APP}
cf rename ${APP}-new ${APP}

cf set-env ${APP} OPTIMIZE_MEMORY true
cf map-route ${APP} apps.internal --hostname ${APP}
cf add-network-policy ${APP} --destination-app txl-login-controller-service-staging
cf add-network-policy txl-transaction-service-staging --destination-app ${APP}
cf add-network-policy txl-loyalty-service-staging --destination-app ${APP}
'use strict';

const i18n = require('./i18n.json');

const codes = [];
//expected positive
codes['00'] = {rc: '00', rm: 'Success'};
codes['01'] = { rc: '01', rm: 'Location ID is not found' };
codes['02'] = { rc: '02', rm: 'National ID invalid' };
codes['03'] = { rc: '03', rm: 'Birthdate invalid' };
codes['06'] = { rc: '06', rm: 'Get KTP failed' };
codes['07'] = { rc: '07', rm: 'KTP already exist' };
codes['08'] = { rc: '08', rm: 'Wrong format file' };
codes['09'] = { rc: '09', rm: 'File not exist' };
codes['10'] = { rc: '10', rm: 'File too large' };
//expected negative
codes['11'] = {rc: '11', rm: 'PIN not match'};
codes['12'] = {rc: '12', rm: 'Unexpected result'};
codes['13'] = { rc: '13', rm: 'Insufficient request parameter' };
codes['14'] = {rc: '14', rm: 'Force logout'};
codes['15'] = {rc: '15', rm: 'Wrong OTP'};
codes['26'] = { rc: '26', rm: 'Failed Reset PIN' };
codes['27'] = { rc: '27', rm: 'Failed Change PIN' };
codes['28'] = { rc: '28', rm: 'Invalid PIN' };
codes['51'] = {rc: '51', rm: 'PIN change disabled'};
//abnormal error
codes['90'] = {rc: '90', rm: 'Email OTP failed'};
codes['91'] = {rc: '91', rm: 'Reach API failed'};
codes['92'] = {rc: '92', rm: 'Throttled'};

function mapCodes(rc, lang, queries) {
	if(lang == null) {
		lang = 'en';
	}
	const mappedRC = codes[rc] ? codes[rc].rc : '';
	let message = replaceParams(i18n[rc][lang.toLowerCase()], queries);
	message = message == null ? replaceParams(i18n[rc]['en'], queries) : message;
	return {rc: mappedRC, rm: message};
}

function replaceParams(url, params) {
	if(url && params) {
		for(let i = 0;i < params.length;i++) {
			url = url.replace('${'+ params[i][0] +'}', params[i][1]);
		}    
	}
	return url;
}

module.exports = {
	i18n: mapCodes,
	codes
}